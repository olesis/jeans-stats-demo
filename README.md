# README #

### What is this repository for? ###
* demo only

### Quick summary ###
* takes row data from file with 10k records. 
* alternatively can create random data with 1k record using `Chance` lib.
* Parse, index and groups.
* Displays graphs with total values
* Allow grouping with other properties dynamically on the fly

* Version 1.0.0

# How do I get set up? #

### Used Node version ###
* `node.js v7.5.0`
### Install all package ###
* `npm install`
### Run application ###
* `npm start`
### Run Tests ###
* `npm run test`

# Libraries #
* AngularJS
* Angular UI Router
* Lodash
* Chance
* amChart
* babel for es6 support
* webpack
* karma
* jasmine
* PhantomJS

# Description #
* From requirements it was understandable that need some statistics solution.
Because data sample file was small. Create some top sellers solution was not reasonable. 
So I created general way to display grouped statistics. It means that you can select any param and group with any other.
Example by country and size or manufacturer and gender and etc. To avoid using tables or creating charts by myself. I used amCharts library. 
And also included some additional visual selections using those charts. 
I still use UI router even there is only one page and one controller. But usually projects are bigger then one page so I selected to use UI router. 
* For this example I took 10k record. Usually front-end do not work with so many data. Aggregation, grouping, filtering for that should be done on in back-end. 
However It was interesting to check performance if it would be done on front-end. And because of that I also left some debug information on top, that shows data parsing time.
To optimise performance I do group all data by all properties in one loop. Then use them. In result I found performance still acceptable even with so many data.
* Because of re-usability I used few angular components and config approach. Where parent gives object with all info needed for children. 
* I did not install SASS/LESS, but I prefer using them. And used just minimalist style here. 
* Sorry had no time to add comments.