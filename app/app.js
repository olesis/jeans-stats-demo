import angular from 'angular';
import uirouter from 'angular-ui-router';

import * as dashboard from './pages/dashboard';
import services from './services';
import components from './components';

angular.module('app', [uirouter, services, components, dashboard.name])
    .config( /* @ngInject */ ($stateProvider, $urlRouterProvider) => {
        $urlRouterProvider.otherwise('/dashboard');

        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                template: dashboard.template,
                controller: dashboard.controller,
                controllerAs: 'ctrl',
                resolve: dashboard.resolve
            });
    });
