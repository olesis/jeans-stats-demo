import template from './am-chart.html';
import controller from './am-chart.controller';

export default {
    bindings: {
        data: '='
    },
    template,
    controller
};