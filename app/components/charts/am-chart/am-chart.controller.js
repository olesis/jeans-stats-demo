import 'amcharts3/amcharts/amcharts.js';
import 'amcharts3/amcharts/serial.js';
import Chance from 'chance';
import values from 'lodash.values';
import forEach from 'lodash.foreach';
import get from 'lodash.get';
import map from 'lodash.map';

/* @ngInject */
export default class {
    constructor($scope, $filter, $timeout) {
        window.amChart = this;

        this._$scope = $scope;
        this._$filter = $filter;
        this._$timeout = $timeout;
        this.id = new Chance().guid();
    }

    get hasData() {
        return this.data && this.data.list;
    }

    $onInit() {
        this._$scope.$watch(() => this.hasData, (val) => {
            if (val) {
                this.render();
            }
        });
    }

    render() {
        this.data.loading = true;
        this.list = this.parseData(this.data.list);

        this._$timeout(() => {
            this.chartConfig = this.getConfig();
            this.chart = AmCharts.makeChart(this.id, this.chartConfig);
            this.data.render = this.render.bind(this);
            this.data.loading = false;
        });
    }

    parseData(list) {
        list = values(list);
        return this.orderList(list);
    }

    orderList(list) {
        const orderBy = this.data.orderBy || 'total';

        return this._$filter('orderBy')(list, orderBy, !this.data.orderBy);
    }

    groupChildren(list, by = 'country') {
        if (!list[0].children) {
            list = forEach(list, val => {
                val.children = val.gr(val.list);
            });
        }

        const graphLabels = {};

        list = map(list, val => {
            const items = values(val.children[by]);
            const copy = {
                children: val.children,
                total: val.total,
                gr: val.gr,
                label: val.label
            };
            items.forEach(item => {
                copy[item.label] = item.total;
                graphLabels[item.label] = this.getGraphLabels(item);
            });
            return copy;
        });
        return { list, graphLabels: values(graphLabels) };
    }

    getGraphLabels(item) {
        return {
            'balloonText': '<b>[[title]]</b><br><span style="font-size:14px">[[category]]: <b>[[value]]</b></span>',
            'fillAlphas': 0.8,
            'labelText': '[[value]]',
            'lineAlpha': 0.3,
            'title': item.label,
            'color': '#000000',
            'type': 'column',
            'topRadius': this.data.topRadius,
            'valueField': item.label,
        }
    }

    setColors(list) {
        const colors = [ '#FF0F00', '#FF6600', '#FF9E01', '#FCD202', '#F8FF01', '#B0DE09', '#04D215', '#0D8ECF',
            '#0D52D1', '#2A0CD0', '#8A0CCF', '#CD0D74', '#754DEB', '#DDDDDD', '#999999', '#333333', '#000000' ];
        list.forEach((val, key) => {
            val.color = get(this.data, `colorsByLabel.${val.label}`) || colors[key];
        });
    }

    getConfig() {
        let grouped = {};

        if (this.data.groupBy) {
            grouped = this.groupChildren(this.list, this.data.groupBy);
            this.list = grouped.list;
        } else {
            this.setColors(this.list);
        }

        const result = {
            'type': 'serial',
            'theme': 'light',
            'startDuration': sessionStorage.useAnimation === 'false' ? 0 : 1,
            'rotate': this.data.type === 'bar',
            'depth3D': this.data.show3D ? 10 : 0,
            'angle': 50,
            'legend': {
                'horizontalGap': 10,
                'useGraphSettings': true,
                'markerSize': 10
            },
            'dataProvider': this.list,
            'valueAxes': [ {
                'stackType': this.data.stackType || 'regular',
                'axisAlpha': 0,
                'gridAlpha': 0
            } ],
            'graphs': grouped.graphLabels || [{
                'balloonText': '<b>[[title]]</b><br><span style="font-size:14px">[[category]]: <b>[[value]]</b></span>',
                'fillAlphas': 0.8,
                'labelText': '[[value]]',
                'lineAlpha': 0.3,
                'title': 'Total',
                'type': 'column',
                'color': '#000000',
                'valueField': 'total',
                'topRadius': this.data.topRadius,
                colorField: 'color'
            }],
            'categoryField': 'label',
            'categoryAxis': {
                'gridPosition': 'start',
                'axisAlpha': 0,
                'gridAlpha': 0,
                'position': 'left',
                labelRotation: 45
            }
        };

        if (this.data.multiColumns) {
            delete result.valueAxes[0].stackType;
        }

        return result;
    }
}