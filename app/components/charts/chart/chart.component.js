import template from './chart.html';
import controller from './chart.controller';

export default {
    bindings: {
        data: '='
    },
    template,
    controller
};