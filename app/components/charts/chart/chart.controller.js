/* @ngInject */
export default class {
    constructor() {
        window.chart = this;
        this.availableGroups = [
            { label: 'Total' },
            { value: 'manufacturer', label: 'Manufacturer' },
            { value: 'color', label: 'Color' },
            { value: 'size', label: 'Size' },
            { value: 'country', label: 'Country' },
            { value: 'gender', label: 'Gender' },
            { value: 'month', label: 'Month' },
            { value: 'style', label: 'Style' }
        ];
    }

    show3D() {
        this.data.show3D = !this.data.show3D;
        this.data.render();
    }

    show100() {
        this.data.stackType = this.data.stackType === '100%' ? null : '100%';
        this.data.render();
    }

    showBar() {
        this.data.type = this.data.type === 'bar' ? 'column' : 'bar';
        this.data.render();
    }

    showMultiColumns() {
        this.data.multiColumns = !this.data.multiColumns;
        this.data.render();
    }

    changeGroup() {
        this.data.render();
    }

    showRadius() {
        const availableRadius = [0, 0.5, 1];
        const index = availableRadius.indexOf(this.data.topRadius);
        this.data.topRadius = availableRadius[ index + 1 ];
        this.data.render();
    }
}