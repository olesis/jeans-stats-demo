import angular from 'angular';
import amChart from './charts/am-chart/am-chart.component';
import chart from './charts/chart/chart.component';

export default angular.module('app.components', [])
    .component('amChart', amChart)
    .component('chart', chart)
    .name;