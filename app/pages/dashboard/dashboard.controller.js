/* @ngInject */
export default class{
    constructor($scope, ordersSummaryService) {
        window.test = this;
        this._ordersSummaryService = ordersSummaryService;
        this.time = [];
        this.animation = (sessionStorage.useAnimation !== 'false').toString();

        this.charts = [
            {
                name: 'month',
                title: 'Months in 2016',
                type: 'bar',
                orderBy: 'time',
                groupBy: 'country'
            },
            {
                name: 'country',
                title: 'Country',
                type: 'column',
                groupBy: 'manufacturer',
                show3D: true,
                stackType: "100%",
            },
            {
                name: 'manufacturer',
                title: 'Manufacturer',
                type: 'bar',
                show3D: true,
                groupBy: 'country',
                topRadius: 1
            },
            {
                name: 'color',
                title: 'Color',
                type: 'column',
                show3D: true,
                multiColumns: true,
                colorsByLabel: {
                    'Light Blue': '#3d6f84',
                    'Dark Blue': '#003366',
                    'Black': '#483C32',
                    'Red': '#C23B22',
                    'Yellow': '#F0E130'
                }
            },
            {
                name: 'size',
                title: 'Size',
                type: 'column',
                groupBy: 'gender',
                show3D: true,
                multiColumns: true,
                topRadius: 0
            },
            {
                name: 'gender',
                title: 'Gender',
                type: 'bar',
                groupBy: 'country',
                show3D: true
            }
        ];

    }

    $onInit() {
        this._ordersSummaryService.getData().then( response => {
            this.time.push('From file 10k -> ' + this._ordersSummaryService.time + ' sec');

            this.setData(response);
        });
    }

    loadData() {
        this.$onInit();
    }

    createData() {
        let result = this._ordersSummaryService.createData();
        result = this._ordersSummaryService.groupData(result);
        this.setData(result);
        this.time.push('Random data 1k -> ' + this._ordersSummaryService.time + ' sec');
    }

    setData(response) {
        this.charts.forEach(val => {
            val.list = response[val.name];
        });
    }

    useAnimation() {
        sessionStorage.useAnimation = this.animation;
    }
};

