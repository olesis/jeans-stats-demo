import angular from 'angular';
import template from './dashboard.html';
import controller from './dashboard.controller';

const name = angular.module('app.dashboard', [])
	.controller('Dashboard', controller)
	.name;

export { template, controller, name };