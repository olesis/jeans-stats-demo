import angular from 'angular';
import ordersSummary from './orders-summary.service';

export default angular.module('app.services', [])
	.service('ordersSummaryService', ordersSummary)
	.name;