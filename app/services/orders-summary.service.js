import Chance from 'chance';

/* @ngInject */
export default ($http, $filter) => {
    return {
        createData(nr = 1000) {
            const chance = new Chance();
            const country = ['Germany', 'Austria', 'Belgium', 'United Kingdom', 'France'];
            const manufacturer = ['The Hipster Jeans Company', 'Denzil Jeans', 'Wrangled Jeans'];
            const color = ['Red', 'Light Blue', 'Black', 'Dark Blue', 'Yellow'];
            const style = ['Boot Cut', 'Relaxed', 'Skinny', 'Regular'];
            const size = ['16', '18', '28/30', '32/32'];

            const result = [];

            for(let i = 0; i < nr; i++) {
            	const gender = chance.gender();
            	const sizeChance = {
            		Male: [0, 5, 32, 45],
            		Female: [35, 45, 25, 13]
            	};

                const item = {
                    country: chance.weighted(country, [45, 12, 8, 37, 32]),
                    date: chance.date({year: 2016}),
                    manufacturer: chance.pickone(manufacturer),
                    gender,
                    color: chance.weighted(color, [5, 40, 21, 45, 3]),
                    style: chance.pickone(style),
                    size: chance.weighted(size, sizeChance[gender]),
                    total: chance.integer({min: 1, max: 20})
                };
                result.push(item);
            };

            this.response = result;
            return result;
        },
        getData() {
            return $http.get('api/data.json').then( response => {
                this.result = this.groupData(response.data);
                return this.result;
            });
        },
        result: {},
        groupBy(result, item, type) {
            if (!result[type]) {
                result[type] = {};
            }

            if (!result[type][item[type]]) {
                result[type][item[type]] = {
                    list: [],
                    total: 0,
                    label: item[type],
                    gr: this.groupData.bind(this)
                };
            }

            if (type === 'month') {
                result[type][item[type]].label = item.label;
                result[type][item[type]].time = item.date.getTime();
            }

            result[type][item[type]].total += item.total;
            result[type][item[type]].list.push(item);
            return result;
        },
        groupData(list) {
            // console.time('GROUP');

            const begin = Date.now();
            const result = {};
            list.forEach(item => {
                item.date = new Date(item.date);
                item.month = $filter('date')(item.date, 'yyyy-MM');
                item.label = $filter('date')(item.date, 'MMMM');

                this.groupBy(result, item, 'manufacturer');
                this.groupBy(result, item, 'size');
                this.groupBy(result, item, 'color');
                this.groupBy(result, item, 'style');
                this.groupBy(result, item, 'gender');
                this.groupBy(result, item, 'country');
                this.groupBy(result, item, 'month');
            });

            const end = Date.now();

            this.time = (end - begin) / 1000;

            // console.timeEnd('GROUP');
            return result;
        }
    };
};
