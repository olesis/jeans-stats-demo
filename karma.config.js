var webpackConfig = require('./webpack.config.js');
// webpackConfig.entry = {};

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    plugins: ['karma-babel-preprocessor', 'karma-webpack', 'karma-jasmine', 'karma-phantomjs-launcher'],
    reporters: ['progress'],
    port: 7777,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './app/bundle.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './tests/**/*.js'],

    preprocessors: {
      './app/bundle.js': ['webpack'],
      './tests/**/*.spec.js': ['babel']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true
    }
  });
}