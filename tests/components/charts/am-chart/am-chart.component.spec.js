describe('amChart Directive -', () => {
    let $scope;
    let $compile;
    let $element;
    let $ctrl;
    let isolateScope;
    const children = {
        size: {
            small: { label: 'small', total: 4 },
            big: { label: 'big', total: 7 }
        }
    };

    const mockData = {
        name: 'country',
        list: {
            aaa: {label: 'Austria', total: 2, test: 10, children },
            bbb: {label: 'France', total: 15, test: 60, children },
            ccc: {label: 'Italy', total: 8, test: 70, children }
        }
    };

    const compile = () => {
        $element = $compile('<am-chart data="chartData"></am-chart>')($scope);
        isolateScope = $element.isolateScope();
        $ctrl = isolateScope.$ctrl;
        $scope.$digest();
    }

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.module($provide => {
        const $timeout = (callback) => {
            callback();
        };
        $provide.value('$timeout', $timeout);
    }));

    beforeEach(angular.mock.inject(($rootScope, _$compile_) => {
        $scope = $rootScope.$new();
        $compile = _$compile_;
    }));

    describe('When show total only -', () => {
        beforeEach(() => {
            $scope.chartData = mockData;
            compile();
        });

        it('Should have data 1', () => {
            expect($ctrl.data).toBe($scope.chartData);
        });

        it('Should have id', () => {
            expect($ctrl.id).toEqual(jasmine.any(String));
        });

        it('Should create list from data.list', () => {
            expect($ctrl.list.length).toBe(3);
        });

        it('Should set color if groupBy is not defined', () => {
            $ctrl.list.forEach(item => {
                expect(item.color).toEqual(jasmine.any(String));
                expect(item.color[0]).toBe('#');
                expect(item.color.length).toBe(7);
            });
        });

        it('Should order list by total with reverse', () => {
            expect($ctrl.list[0].total).toBe(15);
            expect($ctrl.list[2].total).toBe(2);
        });

        it('Should order list by given orderBy value with no reverse', () => {
            $scope.chartData.orderBy = 'test';
            $ctrl.render();

            expect($ctrl.list[0].total).toBe(2);
            expect($ctrl.list[2].total).toBe(8);
        });
    });

    describe('When groupBy is defined -', () => {
        beforeEach(() => {
            $scope.chartData = mockData;
            $scope.chartData.groupBy = 'size';
            compile();
        });

        it('Should not set color', () => {
            $ctrl.list.forEach(item => {
                expect(item.color).toBeUndefined();
            });
        });

        it('Should set totals from children', () => {
            $ctrl.list.forEach(item => {
                expect(item.small).toBe(4);
                expect(item.big).toBe(7);
            });
        });

        it('Should render if values changed', () => {
            spyOn($ctrl, 'render');
            $scope.chartData.list = {};
            $scope.$digest();
            expect($ctrl.render).toHaveBeenCalled();
        });

    });
});