describe('Chart Directive', () => {
    let $scope;
    let $element;
    let $ctrl;
    let isolateScope;

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.module($provide => {
        const $timeout = (callback) => {
            callback();
        };
        $provide.value('$timeout', $timeout);
    }));

    beforeEach(angular.mock.inject(($rootScope, $compile) => {
        $scope = $rootScope.$new();
        $scope.chartData = {
            name: 'country',
            list: {
                aaa: {label: 'Austria', total: 2},
                bbb: {label: 'France', total: 5}
            }
        };

        $element = $compile('<chart data="chartData"></chart>')($scope);
        isolateScope = $element.isolateScope();
        $ctrl = isolateScope.$ctrl;
        $scope.$digest();

        spyOn($ctrl.data, 'render');
    }));

    it('Should have data', () => {
        expect($ctrl.data).toBe($scope.chartData);
    });

    it('Should have availableGroups', () => {
        expect($ctrl.availableGroups).toEqual(jasmine.any(Array));
    });

    it('Should have render Function from child component', () => {
        expect($ctrl.data.render).toEqual(jasmine.any(Function));
    });

    it('Should toggle show3D and call render', () => {
        $ctrl.show3D();
        expect($ctrl.data.show3D).toBe(true);
        expect($ctrl.data.render).toHaveBeenCalled();
        $ctrl.show3D();
        expect($ctrl.data.show3D).toBe(false);
    });

    it('Should toggle stackType and call render', () => {
        $ctrl.show100();
        expect($ctrl.data.stackType).toBe('100%');
        expect($ctrl.data.render).toHaveBeenCalled();
        $ctrl.show100();
        expect($ctrl.data.stackType).toBe(null);
    });

    it('Should toggle type and call render', () => {
        $ctrl.showBar();
        expect($ctrl.data.type).toBe('bar');
        expect($ctrl.data.render).toHaveBeenCalled();
        $ctrl.showBar();
        expect($ctrl.data.type).toBe('column');
    });

    it('Should toggle showMultiColumns and call render', () => {
        $ctrl.showMultiColumns();
        expect($ctrl.data.multiColumns).toBe(true);
        expect($ctrl.data.render).toHaveBeenCalled();
        $ctrl.showMultiColumns();
        expect($ctrl.data.multiColumns).toBe(false);
    });

    it('Should call render on changeGroup', () => {
        $ctrl.changeGroup();
        expect($ctrl.data.render).toHaveBeenCalled();
    });

    it('Should change topRadius number and call render', () => {
        $ctrl.showRadius();
        expect($ctrl.data.topRadius).toBe(0);
        expect($ctrl.data.render).toHaveBeenCalled();
        $ctrl.showRadius();
        expect($ctrl.data.topRadius).toBe(0.5);
        $ctrl.showRadius();
        expect($ctrl.data.topRadius).toBe(1);
        $ctrl.showRadius();
        expect($ctrl.data.topRadius).toBeUndefined();
    });
});