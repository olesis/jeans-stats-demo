describe('Dashboard Controller -', () => {
    let ordersSummaryService;
    let $scope;
    let $ctrl;

    beforeEach(angular.mock.module('app'));

    beforeEach(angular.mock.inject(($rootScope, $controller, _ordersSummaryService_) => {
        ordersSummaryService = _ordersSummaryService_;

        spyOn(ordersSummaryService, 'createData').and.callThrough();
        spyOn(ordersSummaryService, "getData").and.callFake(() => ({
            then: (callback) => {
                const response = ordersSummaryService.createData(3)
                callback(ordersSummaryService.groupData(response));
            }
        }));

        $scope = $rootScope.$new();
        $ctrl = $controller('Dashboard', { $scope, ordersSummaryService });

        spyOn($ctrl, 'setData').and.callThrough();

        $ctrl.$onInit();
    }));


    it('Should get data on init', () => {
        expect(ordersSummaryService.getData).toHaveBeenCalled();
    });

    it('Should have animation value', () => {
        expect($ctrl.animation).toBe('true');
    });

    it('Should have list of charts', () => {
        expect($ctrl.charts).toEqual(jasmine.any(Array));
    });

    it('Should each charts item have list property', () => {
        $ctrl.charts.forEach(item => {
            expect(item.list).toEqual(jasmine.any(Object));
        });
    });

    it('Should call to get data from api', () => {
        $ctrl.loadData();
        expect(ordersSummaryService.getData.calls.count()).toBe(2);
        expect($ctrl.setData.calls.count()).toBe(2);
    });

    it('Should call to get random data and set to charts', () => {
        $ctrl.createData();
        expect(ordersSummaryService.createData).toHaveBeenCalled();
        expect($ctrl.setData.calls.count()).toBe(2);
    });
});